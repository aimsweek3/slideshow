package com.example.lynn.slideshow;

import android.view.MotionEvent;
import android.view.View;

import static com.example.lynn.slideshow.MainActivity.*;

/**
 * Created by lynn on 6/23/2016.
 */
public class MyListener implements View.OnTouchListener {
    private int startX;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            startX = (int)(event.getRawX());
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            int endX = (int)(event.getRawX());

            if (startX < endX)
                index++;
            else
                index--;

            if (index < 0)
                index = drawables.length-1;
            else if (index > drawables.length-1)
                index = 0;

            view.setImageDrawable(drawables[index]);

            myView.setBackground(myView.createGradientDrawable());
        }

        return true;
    }

}
