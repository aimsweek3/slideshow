package com.example.lynn.slideshow;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;

import static com.example.lynn.slideshow.MainActivity.*;

/**
 * Created by lynn on 6/23/2016.
 */
public class MyView extends LinearLayout {

    public GradientDrawable createGradientDrawable() {
        int red = (int)(256*Math.random());
        int green = (int)(256*Math.random());
        int blue = (int)(256*Math.random());

        int startColor = Color.argb(255, red, green, blue);

        red = (int)(256*Math.random());
        green = (int)(256*Math.random());
        blue = (int)(256*Math.random());

        int endColor = Color.argb(255, red, green, blue);

        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {startColor,endColor});
        gradientDrawable.setCornerRadius(0f);

        return(gradientDrawable);
    }

    public MyView(Context context) {
        super(context);

        setBackground(createGradientDrawable());

        drawables = new Drawable[13];

        drawables[0] = getResources().getDrawable(R.drawable.bananas);
        drawables[1] = getResources().getDrawable(R.drawable.bear);
        drawables[2] = getResources().getDrawable(R.drawable.blueberries);
        drawables[3] = getResources().getDrawable(R.drawable.boat);
        drawables[4] = getResources().getDrawable(R.drawable.building);
        drawables[5] = getResources().getDrawable(R.drawable.butterfly);
        drawables[6] = getResources().getDrawable(R.drawable.cake);
        drawables[7] = getResources().getDrawable(R.drawable.car);
        drawables[8] = getResources().getDrawable(R.drawable.chess);
        drawables[9] = getResources().getDrawable(R.drawable.cloud);
        drawables[10] = getResources().getDrawable(R.drawable.colorado);
        drawables[11] = getResources().getDrawable(R.drawable.dog);
        drawables[12] = getResources().getDrawable(R.drawable.duck);

        view = new ImageView(context);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(500,500);

        view.setLayoutParams(layoutParams);

        view.setImageDrawable(drawables[0]);

        view.setTranslationX(500);
        view.setTranslationY(200);

        view.setOnTouchListener(listener);

        addView(view);
    }

}
